export class Breakable extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        const breakableTexture = scene.textures.get('breakable')
        const frame = Math.floor(Math.random() * (breakableTexture.frameTotal - 2)) + 1
        super(scene, x, y, 'breakable', frame);
        this.xMomentum = 0
        this.broken = false
        this.yMomentum = 0
        this.visible = true
        this.body = new Phaser.Physics.Arcade.Body(scene.physics.world, this);
        scene.physics.world.enableBody(this, 0)
        this.depth = 5
        this.x = (x * 32) + 16
        this.y = (y * 32) + 16
        this.activate = () => {
            if (this.broken === false) {
                this.broken = true
                scene.sound.play('boxBreak')
                this.setTexture('breakable', 0)
                const drop = Math.floor(Math.random() * 128)

                if (drop < 64) {
                    //Nothing
                } else if (drop < 112) {
                    scene.gameState.addMoney(Math.floor(Math.random() * 3) + 1)
                } else if (drop < 126) {
                    scene.gameState.addMoney(Math.floor(Math.random() * 12) + 1)
                } else if (drop === 126) {
                    if (scene.gameState.itemIsAvailable("Forest Boots")) {
                        scene.gameState.buyItem("Forest Boots")
                    scene.gameState.messages.push('You found the Forest Boots while searching about. You feel a bit quicker.')

                    } else {
                        scene.gameState.addMoney(25)
                    }
            } else if (drop === 127) {
                if (scene.gameState.itemIsAvailable("Shopkeeper Charm")) {
                    scene.gameState.buyItem("Shopkeeper Charm")
                    scene.gameState.messages.push('You found the Shopkeeper\'s Necklace while searching about. Their inventory has expanded.')

                } else {
                    scene.gameState.addMoney(25)
                }
            }
        }
        }
        scene.add.existing(this);
    }
}
