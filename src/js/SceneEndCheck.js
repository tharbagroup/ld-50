import { GameSettings } from './GameSettings.js'
import { GameState } from './GameState.js'

export let SceneEndCheck = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneEndCheck" })
                let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
               //this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (data) {
        if (!(data.gameSettings instanceof GameSettings)) {
            throw 'GameSettings undefined.'
        } else {
            this.gameSettings = data.gameSettings
        }
        if (!(data.gameState instanceof GameState)) {
            throw 'GameState undefined.'
        } else {
            this.gameState = data.gameState
        }

    },
    preload: () => { },
    create: function () {
        let menuText = this.add.text(
            this.sys.game.canvas.width / 2,
            32,
            `Forest ${this.gameState.day}`,
            {
                fontSize:64,
                color: "#55FF55",
                fontStyle: "bold"
            }
        ).setOrigin(0.5)

        if (this.gameState.lead < 0) {
            //Game Over!
            this.scene.start("SceneEnd", { gameState: this.gameState, gameSettings: this.gameSettings })
        }

        let messages = []

        messages.push('As you push your way through the clearing towards the next forest.')
        messages.push('you spot a door behind some bushes. If you were to hide here, your persuers')
        messages.push('would be very unlikely to find you.')
        messages.push('')

        if (this.gameState.keyPurchased === true) {
        messages.push('Would you like to try your key in the door?')
        let startButton = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height - 48,
            "Continue",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#88ff88",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => { this.scene.start("SceneGameWin", { gameState: this.gameState, gameSettings: this.gameSettings }) })
        } else {

            messages.push('Unfortunately the door is locked.')

            let startButton = this.add.text(
                this.sys.game.canvas.width / 2,
                this.sys.game.canvas.height - 48,
                "Continue",
                {
                    fontSize: 32,
                    color: "#FFFFFF",
                    fontStyle: "bold",
                    backgroundColor: "#88ff88",
                    padding: { x: 10, y: 10 },
    
                }
            )
                .setOrigin(0.5)
                .setInteractive({ useHandCursor: true })
                .on('pointerdown', () => { this.scene.start("SceneDayStart", { gameState: this.gameState, gameSettings: this.gameSettings }) })
        }

        const printMessage = (messageLine, i, skipSpace = false) => {
            if (Array.isArray(messageLine)) {
                messageLine.forEach(line => {
                    printMessage(line, true)
                })
                i++
            } else {
                this.add.text(
                    this.sys.game.canvas.width / 2,
                    this.sys.game.canvas.height / 4 + (32 * i),
                    messageLine,
                    {
                        fontSize: 32,
                        color: "#FF0000",
                        fontStyle: "bold"
                    }
                ).setOrigin(0.5, 0)
                i++
                if (skipSpace !== true) {
                    i++

                }   

            }
        }
        let i = 0
        messages.forEach(message => {
            printMessage(message, i++)


        })




        


        this.cameras.main.fadeIn(1000, 0, 0, 0)

    },
    startGame: function () {
    },
    update: function () {

    }
})