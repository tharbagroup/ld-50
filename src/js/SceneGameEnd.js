import { GameSettings } from './GameSettings.js'
import { GameState } from './GameState.js'

export let SceneGameEnd = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneGameEnd" })
                let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
               //this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (data) {
        if (!(data.gameSettings instanceof GameSettings)) {
            throw 'GameSettings undefined.'
        } else {
            this.gameSettings = data.gameSettings
        }
        if (!(data.gameState instanceof GameState)) {
            throw 'GameState undefined.'
        } else {
            this.gameState = data.gameState
        }

    },
    preload: () => { },
    create: function () {

        let menuText = this.add.text(
            this.sys.game.canvas.width / 2,
            32,
            `RUN OVER`,
            {
                fontSize:64,
                color: "#FF5555",
                fontStyle: "bold"
            }
        ).setOrigin(0.5)



        let messages = []

        messages.push('You were caught in the open while travelling between forests.')
        if (this.gameState.availableItems.length === 0) {
            messages.push(`You have all the collectable items!`)
        } else if (this.gameState.ownedItems.length > 0) {
        messages.push(`You have collected ${this.gameState.ownedItems.length} items so far`)
        messages.push(`You have ${this.gameState.availableItems.length} items left to collect`)
        } else if (this.gameState.ownedItems.length === 0) {
        messages.push(`Try collecting or purchasing items to help on your next run.`)
        }
        messages.push('')
        if (this.gameState.day === this.gameState.bestDay) {
            messages.push(`You made it through ${this.gameState.day -1} forests, your best run yet!`)
        } else {
            messages.push(`You made it through ${this.gameState.day -1} forests, your record is ${this.gameState.bestDay}`)
        }
        messages.push(`You have attempted to escape ${this.gameLoop} times.`)


        const printMessage = (messageLine, i, skipSpace = false) => {
            if (Array.isArray(messageLine)) {
                messageLine.forEach(line => {
                    printMessage(line, true)
                })
                i++
            } else {
                this.add.text(
                    this.sys.game.canvas.width / 2,
                    this.sys.game.canvas.height / 4 + (32 * i),
                    messageLine,
                    {
                        fontSize: 32,
                        color: "#FF0000",
                        fontStyle: "bold"
                    }
                ).setOrigin(0.5, 0)
                i++
                if (skipSpace !== true) {
                    i++

                }

            }
        }
        let i = 0
        messages.forEach(message => {
            printMessage(message, i++)


        })




        let startButton = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height - 48,
            "Attempt To Escape Again",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#88ff88",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => {
                this.gameState.day = 1
                this.gameState.gameLoop++
                this.gameState.money = this.gameState.defaultMoney
                this.gameState.lead = this.gameState.defaultLead
                this.scene.start("SceneDay", { gameState: this.gameState, gameSettings: this.gameSettings }) 
            })


        this.cameras.main.fadeIn(1000, 0, 0, 0)

    },
    startGame: function () {
    },
    update: function () {
         let cursors = this.input.keyboard.createCursorKeys()

            if (cursors.space.isDown) {
                 this.scene.start("SceneDay", { gameState: this.gameState, gameSettings: this.gameSettings })
            }

    }
})