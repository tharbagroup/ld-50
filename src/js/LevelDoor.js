export class LevelDoor extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {

        super(scene, x, y, 'door', null)
        this.xMomentum = 0
        this.yMomentum = 0
        this.visible = true
        this.body = new Phaser.Physics.Arcade.Body(scene.physics.world, this);
        this.body.setImmovable()
        scene.physics.world.enableBody(this, 0)
        this.depth = 5
        this.x = (x * 32) + 16
        this.y = (y * 32) + 16

        this.activate = () => {
            scene.gameState.day++
            if (scene.gameState.day > scene.gameState.bestDay) {
                scene.gameState.bestDay = scene.gameState.day
            }
            scene.scene.stop('SceneUI')
            scene.scene.stop('SceneBackground')
            const elapsedTime = (performance.now() - scene.gameState.levelStart) / 60000
            scene.gameState.lead -= Math.floor(elapsedTime)
            if (Math.random() > (elapsedTime - Math.floor(elapsedTime))) {
                scene.gameState.lead--
            }
            if (scene.gameState.day === 10) {
            scene.scene.start("SceneEndCheck", { gameState: scene.gameState, gameSettings: scene.gameSettings })

            } else {
            scene.scene.start("SceneDayStart", { gameState: scene.gameState, gameSettings: scene.gameSettings })

            }
        }

        scene.add.existing(this);
        this.setCollideWorldBounds(true)
    }
}
