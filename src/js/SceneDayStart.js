import { GameSettings } from './GameSettings.js'
import { GameState } from './GameState.js'

export let SceneDayStart = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneDayStart" })
        let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
                //this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (data) {
        if (!(data.gameSettings instanceof GameSettings)) {
            throw 'GameSettings undefined.'
        } else {
            this.gameSettings = data.gameSettings
        }
        if (!(data.gameState instanceof GameState)) {
            throw 'GameState undefined.'
        } else {
            this.gameState = data.gameState
        }

    },
    preload: () => { },
    create: function () {
        let menuText = this.add.text(
            this.sys.game.canvas.width / 2,
            32,
            `Forest ${this.gameState.day}`,
            {
                fontSize: 64,
                color: "#55FF55",
                fontStyle: "bold"
            }
        ).setOrigin(0.5)

        if (this.gameState.lead <= 0) {
            //Game Over!
            this.scene.start("SceneGameEnd", { gameState: this.gameState, gameSettings: this.gameSettings })
        }

        let messages = []

        if (Math.random() < 0.05) {
            const moneyChange = Math.floor(this.gameState.money * 0.15)
            this.gameState.money -= moneyChange
            messages.push(`${moneyChange} coins have disappeared from your pocket`)
        } else if (Math.random() < 0.05) {
            const moneyChange = Math.floor(this.gameState.money * 0.15)
            this.gameState.money += moneyChange
            messages.push(`${moneyChange} more coins have appeared in your pocket`)
        }

        if (Math.random() < 0.05) {
            const moneyChange = Math.floor(this.gameState.money * 0.15)
            this.gameState.money -= moneyChange
            messages.push(`${moneyChange} coins have disappeared from your pocket`)
        } else if (Math.random() < 0.05) {
            const moneyChange = Math.floor(this.gameState.money * 0.15)
            this.gameState.money += moneyChange
            messages.push(`${moneyChange} more coins have appeared in your pocket`)
        }

        if (Math.random() < 0.05) {
            const timeChange = Math.floor(Math.random() * 4) + 1
            this.gameState.lead -= timeChange
            messages.push(`You got a little lost during your travel, but eventually found your path.`)
        } else if (Math.random() < 0.05) {
            const timeChange = Math.floor(Math.random() * 4) + 1
            this.gameState.lead += timeChange
            messages.push(`${`The path between forests was overgrown, but you had no problem finding your path.`}`)
        }
        if (this.gameState.itemIsAvailable("Spring Water")) {
            if (Math.random() < 0.05) {
                this.gameState.setPrice("Spring Water", 0)
                this.gameState.buyItem("Spring Water")
                messages.push(`You found a fresh water spring and drank from it. You feel refreshed and energized.`)
            }
        }

        while (this.gameState.messages.length > 0) {
            messages.push(this.gameState.messages.pop())
            console.log(this.gameState.messages)
        }

        //Process messages to show
        if (messages.length === 0) {
            messages.push('The trip between forests was uneventful.')
        }


        const printMessage = (messageLine, i, skipSpace = false) => {
            if (Array.isArray(messageLine)) {
                messageLine.forEach(line => {
                    printMessage(line, true)
                })
                i++
            } else {
                this.add.text(
                    this.sys.game.canvas.width / 2,
                    this.sys.game.canvas.height / 4 + (32 * i),
                    messageLine,
                    {
                        fontSize: 32,
                        color: "#FF0000",
                        fontStyle: "bold"
                    }
                ).setOrigin(0.5, 0)
                i++
                if (skipSpace !== true) {
                    i++

                }

            }
        }
        let i = 0
        messages.forEach(message => {
            printMessage(message, i++)


        })




        let startButton = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height - 48,
            "Continue",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#88ff88",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => { this.scene.start("SceneDay", { gameState: this.gameState, gameSettings: this.gameSettings }) })


        this.cameras.main.fadeIn(1000, 0, 0, 0)

    },
    startGame: function () {
    },
    update: function () {
        let cursors = this.input.keyboard.createCursorKeys()

        if (cursors.space.isDown) {
            this.scene.start("SceneDay", { gameState: this.gameState, gameSettings: this.gameSettings })
        }

    }
})