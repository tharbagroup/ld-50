import { GameSettings } from './GameSettings.js'
import { GameState } from './GameState.js'
import { GameMap } from './GameMap.js'
import { Player } from './Player.js'

export let SceneDay = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneDay" })
        let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
 //              //this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (data) {
        if (!(data.gameSettings instanceof GameSettings)) {
            throw 'GameSettings undefined.'
        } else {
            this.gameSettings = data.gameSettings
        }
        if (!(data.gameState instanceof GameState)) {
            throw 'GameState undefined.'
        } else {
            this.gameState = data.gameState
        }

    },
    preload: function () {
        this.load.spritesheet('player', 'images/player.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.image('shopkeeper', 'images/shopkeeper.png')
        this.load.image('door', 'images/door.png')
        this.load.spritesheet('tiles', 'images/tiles.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.spritesheet('floor', 'images/floor.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.spritesheet('wall', 'images/wall.png', {
            frameWidth: 32,
            frameHeight: 32
        })
        this.load.spritesheet('breakable', 'images/breakable.png', {
            frameWidth: 32,
            frameHeight: 32
        })

        this.load.audio('boxBreak', 'sounds/break.wav')

        //Is this needed?
        this.textures.get('floor').setFilter(1)
    },
    create: function () {

        this.cameras.main.setZoom(3)
        this.gameState.gameMap = new GameMap(this.gameState.day)

        //Player before map, map creates the collider
        //Initialize at 0, 0 so we can place in a valid square at the start of the game
        this.player = new Player(this, 0, 0)
        this.player.depth = 10

        this.gameState.gameMap.draw(this)
        const startPosition = this.gameState.gameMap.getStart()
        this.player.x = startPosition.x
        this.player.y = startPosition.y
        const UI = this.scene.get('SceneUI')
        this.scene.launch('SceneBackground')
        this.scene.sendToBack('SceneBackground')
        this.scene.launch('SceneUI', { gameState: this.gameState, gameSettings: this.gameSettings })
        this.scene.bringToTop('SceneUI')


        const maxWidth = (this.gameState.gameMap.getWidth() * this.cameras.main.zoom + (this.cameras.main.displayWidth / 2))
        const minWidth = -(this.cameras.main.displayWidth / 2)
        const maxHeight = (this.gameState.gameMap.getHeight() * this.cameras.main.zoom + (this.cameras.main.displayHeight / 2))
        const minHeight = -(this.cameras.main.displayHeight / 2)

        this.physics.world.setBounds(GameMap.TILE_WIDTH, GameMap.TILE_HEIGHT, this.gameState.gameMap.getWidth() - (GameMap.TILE_WIDTH * 2), this.gameState.gameMap.getHeight() - (GameMap.TILE_HEIGHT * 2))




        this.cameras.main.setBounds(minWidth, minHeight, maxWidth, maxHeight)
        this.cameras.main.fadeIn(1000, 0, 0, 0)
        this.cameras.main.startFollow(this.player)
        this.gameState.levelStart = performance.now()


    },
    startGame: function () {
    },
    update: function () {
        if (this.gameState.paused != true) {
            let cursors = this.input.keyboard.createCursorKeys()

            if (cursors.left.isDown) {
                this.player.moveLeft()
            }
            else if (cursors.right.isDown) {
                this.player.moveRight()
            }

            if (cursors.up.isDown) {
                this.player.moveUp()
            }
            else if (cursors.down.isDown) {
                this.player.moveDown()
            }

            if (cursors.space.isDown) {
                if (this.gameState.scanning === false) {
                    this.gameState.scanning = true
                    this.gameState.gameMap.checkSurroundings(Math.floor(this.player.x / GameMap.TILE_WIDTH), Math.floor(this.player.y / GameMap.TILE_HEIGHT))
                    this.gameState.scanning = false
                }
            }
        }
        this.player.update()
        this.physics.collide(this.player, this.gameState.gameMap.getWallGroup())
    }
})