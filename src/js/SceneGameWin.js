import { GameSettings } from './GameSettings.js'
import { GameState } from './GameState.js'

export let SceneGameWin = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneGameWin" })
                let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
               //this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (data) {
        if (!(data.gameSettings instanceof GameSettings)) {
            throw 'GameSettings undefined.'
        } else {
            this.gameSettings = data.gameSettings
        }
        if (!(data.gameState instanceof GameState)) {
            throw 'GameState undefined.'
        } else {
            this.gameState = data.gameState
        }

    },
    preload: () => { },
    create: function () {
        this.scene.launch('SceneBackground')
        this.scene.sendToBack('SceneBackground')
        let menuText = this.add.text(
            this.sys.game.canvas.width / 2,
            32,
            `You Managed to Escape!`,
            {
                fontSize:64,
                color: "#5555FF",
                fontStyle: "bold"
            }
        ).setOrigin(0.5)

        if (this.gameState.lead < 0) {
            //Game Over!
            this.scene.start("SceneEnd", { gameState: this.gameState, gameSettings: this.gameSettings })
        }

        let messages = []

        messages.push(`${this.gameState.playerName} Has managed to escape!`)
        messages.push('')
        messages.push(`Thanks for playing Forest Run!`)
        messages.push('')
        messages.push(`A Ludum Dare 50 Entry`)

        const printMessage = (messageLine, i, skipSpace = false) => {
            if (Array.isArray(messageLine)) {
                messageLine.forEach(line => {
                    printMessage(line, true)
                })
                i++
            } else {
                this.add.text(
                    this.sys.game.canvas.width / 2,
                    this.sys.game.canvas.height / 4 + (32 * i),
                    messageLine,
                    {
                        fontSize: 32,
                        color: "#FF0000",
                        fontStyle: "bold"
                    }
                ).setOrigin(0.5, 0)
                i++
                if (skipSpace !== true) {
                    i++

                }

            }
        }
        let i = 0
        messages.forEach(message => {
            printMessage(message, i++)


        })


        this.cameras.main.fadeIn(1000, 0, 0, 0)

    },
    startGame: function () {
    },
    update: function () {
    }
})