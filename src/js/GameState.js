export class GameState {
    constructor(playerName) {
        this.playerName = playerName
        this.day = 1
        this.bestDay = 1
        this.defaultLead = 10
        this.lead = this.defaultLead
        this.paused = false
        this.gameLoop = 1
        this.scanning = false
        this.updateUI = false
        this.levelStart = null
        this.maxSpeed = 100
        this.accelerationRate = 10
        this.frictionRate = 1.1
        this.defaultMoney = 0
        this.money = this.defaultMoney
        this.keyPurchased = false
        this.moneyBoost = 0
        this.messages = []
        this.ownedItems = []
        this.availableItems = [
            {
                itemId: 0, frame: 0, name: 'Golden Coin', description: 'Every time you find money, find an additional coin.', price: 75, onPurchase: () => {
                    this.moneyBoost++
                }
            },
            {
                itemId: 1, frame: 1, name: 'Golden Coin 2', description: 'Every time you find money, find an additional coin.', price: 150, onPurchase: () => {
                    this.moneyBoost++
                }
            },
            {
                itemId: 2, frame: 2, name: 'Forest Boots', description: 'Increases maximum speed.', price: 150, onPurchase: () => {
                    this.addMaxSpeed(30)
                }
            },
            {
                itemId: 3, frame: 3, name: 'Shopkeeper Charm', description: 'Shopkeeper sells more items.', price: 0, onPurchase: () => {
                    this.setPrice("Boots of Speed", 250)
                    this.setPrice("Sports Drink", 300)
                    this.setPrice("Strange Key", 50)
                    this.setPrice("Gold Stopwatch", 350)
                }
            },
            {
                itemId: 4, frame: 4, name: 'Boots of Speed', description: 'Increases maximum Speed.', price: 0, onPurchase: () => {
                    this.addMaxSpeed(50)
                }
            },
            {
                itemId: 5, frame: 5, name: 'Spring Water', description: 'Increases maximum Acceleration.', price: 200, onPurchase: () => {
                    this.addAccelerationRate(5)
                }
            },
            {
                itemId: 6, frame: 6, name: 'Sports Drink', description: 'Increases maximum Acceleration. Sugary.', price: 0, onPurchase: () => {
                    this.addAccelerationRate(5)
                }
            },
            {
                itemId: 7, frame: 7, name: 'Strange Key', description: 'Used to Unlock Something. Maybe It Still Works.', price: 0, onPurchase: () => {
                    this.keyPurchased = true
                }
            },
            {
                itemId: 8, frame: 8, name: 'Silver Stopwatch', description: 'Grants you more initial time to escape.', price: 250, onPurchase: () => {
                    this.defaultLead += 4
                }
            },
            {
                itemId: 9, frame: 9, name: 'Gold Stopwatch', description: 'Grants you even more time to escape.', price: 0, onPurchase: () => {
                    this.defaultLead += 4
                }
            },
                        {
                itemId: 10, frame: 10, name: 'Pile of Silver', description: 'Increases your coins at the start of a run.', price: 125, onPurchase: () => {
                    this.defaultMoney += 25
                    this.setPrice("Pile of Gold", 250)
                }
            },
                        {
                itemId: 11, frame: 11, name: 'Pile of Gold', description: 'Increases your coins at the start of a run.', price: 0, onPurchase: () => {
                    this.defaultMoney += 25
                }
            }
        ]
    }

    setPrice(itemName, price) {
        this.availableItems.forEach((item, key, self) => {
            if (item.name === itemName) {
                item.price = price
            }
        })
    }

    itemIsAvailable(itemName) {
        let match = false
        this.availableItems.forEach((item, key, self) => {
            if (item.name == itemName) {
                match = true
            }
        })
        return match
    }

    buyItem(itemName) {
        this.availableItems.forEach((item, key, self) => {
            if (item.name === itemName) {
                //Found match
                this.availableItems.splice(key, 1)
                this.money -= item.price
                this.ownedItems.push(Object.assign({}, item))
                item.onPurchase()
                this.updateUI = true
            }
        })
    }

    addMoney(money) {
        this.money += money + this.moneyBoost
        this.updateUI = true
    }

    addMaxSpeed(change) {
        this.maxSpeed += change
    }

    addAccelerationRate(change) {
        this.accelerationRate += change
    }
}