
import { LevelDoor } from './LevelDoor.js'
import { Shopkeeper } from './Shopkeeper.js'
import { Breakable } from './Breakable.js'
export class GameMap {

    static TILE_WIDTH = 32
    static TILE_HEIGHT = 32

    static DEFAULT_ROOMS = 30

    constructor(day) {
        this.tileCountX = 32
        this.tileCountY = 32
        this.defaultTile = { x: 0, y: 0, textureType: 'wall', texture: 0 }
        //Array of objects with x/y coordinates that need to be retextured based on their surroundings
        this.map = [[{ x: 0, y: 0, textureType: 'floor', texture: 6 }]]

        this.roomGenerator({ x: 0, y: 0 }, GameMap.DEFAULT_ROOMS + (day * 4))

        this.addBorders()

        this.smartTexture(this.getTilesByType('wall'))
        this.transitionRandomTile(this.getTilesByType('wall', 0), 'LevelDoor')

        if (Math.random() > 0.5) {
            this.transitionRandomTile(this.getTilesByType('floor', 0), 'Shopkeeper')
        }

        const floorTiles = this.getTilesByType('floor', 0)
        for (let i = 0; i < floorTiles.length; i++) {
            if (Math.random() < 0.02) {
                this.map[floorTiles[i].y][floorTiles[i].x].textureType = 'Breakable'
            } 
        }
    }


    transitionRandomTile(tiles, obj) {
        let floorTile = {textureType: 'No'}
        let tile = {}
        while (floorTile.textureType != 'floor') {
            tile = tiles[Math.floor(Math.random() * tiles.length)]
            if (tile.y + 1 >= this.map.length) {
                continue
            }
            if (this.map[tile.y + 1][tile.x].textureType == 'floor')
            {
                break
            }
            //Keep getting random tiles until we get one with floor below it so we know it is accessible
        }
        tile.textureType = obj
        tile.texture = 0
    }



    getTilesByType(type, sprite = null) {
        let wallTiles = []
        for (let y = 0; y < this.map.length; y++) {
            for (let x = 0; x < this.map[0].length; x++) {
                this.map[y][x].x = x
                this.map[y][x].y = y
                if (this.map[y][x].textureType === type) {
                    if (sprite === null || this.map[y][x].texture == sprite) {
                        wallTiles.push(this.map[y][x])
                    }
                }
            }
        }
        return wallTiles
    }

    roomGenerator(start, rooms) {
        const hallwayDirection = Math.floor(Math.random() * 4)
        let pointer = start
        switch (hallwayDirection) {
            case 0:
                //Right
                pointer = this.generateRoom(pointer, Math.floor(Math.random() * 25) + 5, Math.floor(Math.random() * 3) + 3)
                break
            case 1:
                //Down
                pointer = this.generateRoom(pointer, Math.floor(Math.random() * 3) + 3, Math.floor(Math.random() * 25) + 5)
                break
            case 2:
                //Left
                pointer = this.generateRoom(pointer, Math.floor(Math.random() * -25) - 5, Math.floor(Math.random() * 3) + 3)
                break
            case 3:
                //Up
                pointer = this.generateRoom(pointer, Math.floor(Math.random() * 3) + 3, Math.floor(Math.random() * -25) - 5)
                break
            default:
                throw 'Invalid direction.'
        }
        rooms--
        if (rooms > 0) {
            this.roomGenerator(pointer, rooms)
        }
    }

    addBorders() {

        this.map.push([])
        this.map.unshift([])
        let maxLength = 0
        this.map.forEach(row => {
            if (row.length > maxLength) {
                maxLength = row.length
            }
        })
        this.map.forEach(row => {
            while (row.length < maxLength) {
                row.push(Object.assign({}, this.defaultTile))
            }
            row.unshift(Object.assign({}, this.defaultTile))
            row.push(Object.assign({}, this.defaultTile))
        })
    }

    activateTile(x, y) {
        if (y > this.tileMap.length || x > this.tileMap[y].length || typeof this.tileMap[y][x] === 'undefined') {
            return
        }
        if (this.tileMap[y][x].hasOwnProperty('activate')) {
            this.tileMap[y][x].activate()
        }
    }

    checkSurroundings(x, y) {
        this.activateTile(x, y)
        this.activateTile(x, y + 1)
        this.activateTile(x - 1, y + 1)
        this.activateTile(x - 1, y)
        this.activateTile(x - 1, y - 1)
        this.activateTile(x, y - 1)
        this.activateTile(x + 1, y - 1)
        this.activateTile(x + 1, y)
        this.activateTile(x + 1, y + 1)
    }

    generateRoom(start, width, height) {
        if (height > 0) {
            while (this.map.length < (start.y + height)) {
                // We can get away with an empty array here, it'll be populated by the width loop
                this.map.push([])
            }
        }

        if (height < 0) {
            let i = height
            let j = 0
            while (start.y + i < 0) {
                // We can get away with an empty array here, it'll be populated by the width loop
                this.map.unshift([])
                i++
                j++
            }
            start.y += j
        }

        // When width < 0, any added rows aren't padded properly. To work around, pad every row after every room is generated.
        let maxLength = 0
        this.map.forEach(row => {
            if (row.length > maxLength) {
                maxLength = row.length
            }
        })

        this.map.forEach(row => {
            while (row.length < maxLength) {
                row.push(Object.assign({}, this.defaultTile))
            }
        })

        if (width < 0) {
            const startWidth = this.map[0].length
            let j = 0
            this.map.forEach(row => {
                let i = width
                while (start.x + i < 0) {
                    row.unshift(Object.assign({}, this.defaultTile))
                    i++
                }
            })
            const endWidth = this.map[0].length
            start.x += endWidth - startWidth
        }

        if (width > 0) {
            this.map.forEach(row => {
                while (row.length < start.x + width + 1) {
                    row.push(Object.assign({}, this.defaultTile))
                }
            })
        }

        if (height > 0) {
            for (let i = 0; i < height; i++) {
                if (width > 0) {
                    for (let j = 0; j < width; j++) {
                        this.map[start.y + i][start.x + j].textureType = 'floor'
                        this.map[start.y + i][start.x + j].texture = this.getFloorTexture()
                    }
                } else if (width < 0) {
                    for (let j = 0; j > width; j--) {
                        this.map[start.y + i][start.x + j].textureType = 'floor'
                        this.map[start.y + i][start.x + j].texture = this.getFloorTexture()
                    }
                }
            }
        } else if (height < 0) {
            for (let i = 0; i > height; i--) {
                if (width > 0) {
                    for (let j = 0; j < width; j++) {
                        this.map[start.y + i][start.x + j].textureType = 'floor'
                        this.map[start.y + i][start.x + j].texture = this.getFloorTexture()
                    }
                } else if (width < 0) {
                    for (let j = 0; j > width; j--) {
                        this.map[start.y + i][start.x + j].textureType = 'floor'
                        this.map[start.y + i][start.x + j].texture = this.getFloorTexture()
                    }

                }
            }
        }
        /*let edgeTiles = []
        const roomWidth = Math.abs(width)
        const roomHeight = Math.abs(height)
        for (let i = 0; i < roomWidth; i++) {
            edgeTiles.push({ x: start.x + i, y: start.y })
            edgeTiles.push({ x: start.x + i, y: start.y + roomHeight })
        }
        for (let i = 0; i < roomHeight; i++) {
            edgeTiles.push({ x: start.x, y: start.y + i })
            edgeTiles.push({ x: start.x + roomWidth, y: start.y + i })
        }
        return edgeTiles[Math.floor(Math.random() * edgeTiles.length)]*/
        return { x: start.x + Math.floor(Math.random() * width), y: start.y + Math.floor(Math.random() * height) }

    }

    getHeight() {
        return GameMap.DEFAULT_ROWS * this.tileCountY
    }

    getWidth() {
        return GameMap.DEFAULT_COLUMNS * this.tileCountX
    }

    smartTexture(textures) {
        if (Array.isArray(textures)) {
            textures.forEach(texture => {
                this._smartTexture(texture)
            })
        } else {
            this._smartTexture(textures)
        }
    }

    _smartTexture(texture) {
        const type = texture.textureType
        let neighbours = 0
        let neighbourIds = []
        let voidTiles = 0
        try {
            if (this.map[texture.y + 1][texture.x].textureType === texture.textureType) {
                neighbours++
                neighbourIds.push(0)
            }
        } catch (e) { voidTiles++ }
        try {
            if (this.map[texture.y - 1][texture.x].textureType === texture.textureType) {
                neighbours++
                neighbourIds.push(2)

            }
        } catch (e) { voidTiles++ }
        try {
            if (this.map[texture.y][texture.x + 1].textureType === texture.textureType) {
                neighbours++
                neighbourIds.push(1)
            }
        } catch (e) { voidTiles++ }
        try {
            if (this.map[texture.y][texture.x - 1].textureType === texture.textureType) {
                neighbours++
                neighbourIds.push(3)
            }
        } catch (e) { voidTiles++ }

        if (voidTiles > 0 && neighbours + voidTiles === 4) {
            this.map[texture.y][texture.x].invisible = true
            return
        }

        if (neighbours === 0) {
            this.map[texture.y][texture.x].physics = true
            //Island
            this.map[texture.y][texture.x].texture = 5
        }

        if (neighbours === 1) {
            this.map[texture.y][texture.x].physics = true

            //Edge piece
            this.map[texture.y][texture.x].texture = 4
            if (neighbourIds.includes(0)) {
                this.map[texture.y][texture.x].angle = 270
            } else if (neighbourIds.includes(1)) {
                this.map[texture.y][texture.x].angle = 180
            } else if (neighbourIds.includes(2)) {
                this.map[texture.y][texture.x].angle = 90
            } else if (neighbourIds.includes(3)) {
                this.map[texture.y][texture.x].angle = 0
            }
        } else if (neighbours === 4) {
            //Fully surrounded, don't draw?
            //texture.texture = 2

            let extendedNeighbours = 0
            let extendedNeighbourIds = []
            try {
                if (this.map[texture.y + 1][texture.x + 1].textureType !== texture.textureType) {
                    extendedNeighbours++
                    extendedNeighbourIds.push(2)
                }
            } catch (e) { }
            try {
                if (this.map[texture.y - 1][texture.x - 1].textureType !== texture.textureType) {
                    extendedNeighbours++
                    extendedNeighbourIds.push(0)

                }
            } catch (e) { }
            try {
                if (this.map[texture.y - 1][texture.x + 1].textureType !== texture.textureType) {
                    extendedNeighbours++
                    extendedNeighbourIds.push(1)
                }
            } catch (e) { }
            try {
                if (this.map[texture.y + 1][texture.x - 1].textureType !== texture.textureType) {
                    extendedNeighbours++
                    extendedNeighbourIds.push(3)
                }
            } catch (e) { }
            if (extendedNeighbours === 0) {
                this.map[texture.y][texture.x].invisible = true
            } else {
                texture.texture = 6
                texture.angle = (extendedNeighbourIds.pop() * 90)
            }

        } else if (neighbours === 3) {
            this.map[texture.y][texture.x].physics = true
            //Edge piece
            this.map[texture.y][texture.x].texture = 0
            if (!neighbourIds.includes(0)) {
                this.map[texture.y][texture.x].angle = 0
            } else if (!neighbourIds.includes(1)) {
                this.map[texture.y][texture.x].angle = 270
            } else if (!neighbourIds.includes(2)) {
                this.map[texture.y][texture.x].angle = 180
            } else if (!neighbourIds.includes(3)) {
                this.map[texture.y][texture.x].angle = 90
            }
        } else if (neighbours === 2) {
            this.map[texture.y][texture.x].physics = true
            //Straight border
            if (Math.abs(neighbourIds[0] - neighbourIds[1]) === 2) {
                texture.texture = 3
                if ((neighbourIds[0] % 2) === 0) {
                    texture.angle = 90
                }
            } else {
                texture.texture = 1
                if (neighbourIds.includes(0)) {
                    //Right
                    if (neighbourIds.includes(1)) {
                        //Down
                        texture.angle = 90
                    } else {
                        //Up
                        texture.angle = 180
                    }
                } else {
                    //Left
                    if (neighbourIds.includes(1)) {
                        texture.angle = 0
                        //Down
                    } else {
                        //Up
                        texture.angle = 270
                    }
                }
            }

        }
    }

    getFloorTexture() {
        //0 is basic
        //1-4 have features
        //5-8 have subtle colour changes

        const weights = {
            0: 100,
            1: 1,
            2: 1,
            3: 1,
            4: 1,
            5: 10,
            6: 10,
            7: 10,
            8: 10
        }
        const weightTotal = Object.values(weights).reduce((partialSum, a) => partialSum + a, 0)

        const random = Math.floor(Math.random() * weightTotal + 1)
        let offset = 0
        for (const [key, value] of Object.entries(weights)) {
            offset += value
            if (offset > random) {
                return key
            }
        }
    }

    /**
     * Get valid starting position for character to make sure we don't spawn in a wall
     * @returns array [x, y]
     */
    getStart() {
        const position = this.getNearestFloorTile({ x: Math.floor(this.map[0].length / 2), y: Math.floor(this.map.length / 2) })
        return { x: position.x * GameMap.TILE_WIDTH + (GameMap.TILE_WIDTH / 2), y: position.y * GameMap.TILE_HEIGHT + (GameMap.TILE_HEIGHT / 2) }

    }

    getNearestFloorTile(position) {
        let offset = 1
        let offsetSign = 1;
        while (true) {
            if (this.map[position.y][position.x].textureType === 'floor') {
                return { x: position.x, y: position.y }
            } else {
                position.x = position.x + (offset * offsetSign)
            }
            if (this.map[position.y][position.x].textureType === 'floor') {
                return { x: position.x, y: position.y }
            } else {
                position.y = position.y + (offset * offsetSign)
                offset++
                offsetSign = offsetSign * -1
            }
        }
    }

    getWallGroup() {
        return this.wallGroup
    }

    draw(game) {
        let rowId = 0
        this.tileMap = []
        this.wallGroup = game.add.group()
        this.map.forEach(row => {
            let columnId = 0
            row.forEach(column => {
                if (!(Array.isArray(this.tileMap[rowId]))) {
                    this.tileMap[rowId] = []
                }
                if (this.map[rowId][columnId].textureType === 'Shopkeeper') {
                    this.tileMap[rowId][columnId] = game.add.existing(new Shopkeeper(game, this.map[rowId][columnId].x, this.map[rowId][columnId].y))
                } else if (this.map[rowId][columnId].textureType === 'LevelDoor') {
                    this.tileMap[rowId][columnId] = game.add.existing(new LevelDoor(game, this.map[rowId][columnId].x, this.map[rowId][columnId].y))
                    game.physics.world.enable(this.tileMap[rowId][columnId])
                    this.wallGroup.add(this.tileMap[rowId][columnId])
                } else if (this.map[rowId][columnId].textureType === 'Breakable') {
                    this.tileMap[rowId][columnId] = game.add.existing(new Breakable(game, this.map[rowId][columnId].x, this.map[rowId][columnId].y))
                } else {
                    if (!this.map[rowId][columnId].hasOwnProperty('invisible')) {

                        this.tileMap[rowId][columnId] = game.add.sprite((columnId * (GameMap.TILE_WIDTH)) + 16, rowId * (GameMap.TILE_HEIGHT) + 16, this.map[rowId][columnId].textureType, this.map[rowId][columnId].texture)
                        if (this.map[rowId][columnId].physics === true) {
                            game.physics.world.enable(this.tileMap[rowId][columnId])
                            this.tileMap[rowId][columnId].body.setImmovable(true)
                            this.wallGroup.add(this.tileMap[rowId][columnId])
                        }
                        if (this.map[rowId][columnId].hasOwnProperty('angle')) {
                            this.tileMap[rowId][columnId].angle = this.map[rowId][columnId].angle
                        }
                    }
                }
                columnId++
            })
            rowId++
        })
    }


}