export class Player extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {

        super(scene, x, y, 'player', 0);
        this.xMomentum = 0
        this.yMomentum = 0
        this.body = new Phaser.Physics.Arcade.Body(scene.physics.world, this);
        scene.physics.world.enableBody(this, 0)
        this.scene = scene
        this.frameTime = 100
        this.frameIndex = 0
        this.nextManualFrame = 1
        scene.add.existing(this);
        this.setCollideWorldBounds(true)

    }

    setXMomentum(change) {
        if (Math.abs(this.xMomentum + change) > this.scene.gameState.maxSpeed) {
                return Math.sign(change) * this.scene.gameState.maxSpeed
        } else {
            return this.xMomentum + change
        }
    }

    calculateNextFrame () {

        const hold = this.frameIndex
        this.frameIndex = this.nextManualFrame
        this.setTexture('player', this.frameIndex)

        if (hold === 1) {
            this.nextManualFrame = 2
        } else if (hold === 2) {
            this.nextManualFrame = 1
        }
        else {
            this.nextManualFrame = 0
        }
    }


    setYMomentum(change) {
        if (Math.abs(this.yMomentum + change) > this.scene.gameState.maxSpeed) {
            return Math.sign(change) * this.scene.gameState.maxSpeed
        } else {
            return this.yMomentum + change
        }
    }

    setAngle() {
        this.angle = Phaser.Math.RadToDeg(Phaser.Math.Angle.Between(0, 0, this.xMomentum, this.yMomentum))
    }

    moveLeft() {
        this.xMomentum = this.setXMomentum(-this.scene.gameState.accelerationRate)
        this.setAngle()
    }

    moveRight() {
        this.xMomentum = this.setXMomentum(this.scene.gameState.accelerationRate)
        this.setAngle()
    }

    moveUp() {
        this.yMomentum = this.setYMomentum(-this.scene.gameState.accelerationRate)
        this.setAngle()
    }

    moveDown() {
        this.yMomentum = this.setYMomentum(this.scene.gameState.accelerationRate)
        this.setAngle()
    }

    stationary() {
        if (this.xMomentum > 0) {
            this.xMomentum = Math.floor(this.xMomentum / this.scene.gameState.frictionRate)
        } else if (this.xMomentum < 0) {
            this.xMomentum = Math.ceil(this.xMomentum / this.scene.gameState.frictionRate)
        }



        if (this.yMomentum > 0) {
            this.yMomentum = Math.floor(this.yMomentum / this.scene.gameState.frictionRate)
        } else if (this.yMomentum < 0) {
            this.yMomentum = Math.ceil(this.yMomentum / this.scene.gameState.frictionRate)
        }
    }

    update() {
        this.frameTime -= ((Math.abs(this.xMomentum) + Math.abs(this.yMomentum)) / 2)
        if (this.frameTime < 0) {
            this.frameTime = 800
            this.calculateNextFrame()
        }
        this.setVelocityX(this.xMomentum)
        this.setVelocityY(this.yMomentum)
        this.stationary()
    }
}
