import { SceneStartGame } from './SceneStartGame.js'
import { SceneGameSettings } from './SceneGameSettings.js' 
import { SceneSplash } from './SceneSplash.js'
import { SceneDayStart } from './SceneDayStart.js'
import { SceneDay } from './SceneDay.js'
import { SceneBackground } from './SceneBackground.js'
import { SceneUI } from './SceneUI.js'
import { SceneShop } from './SceneShop.js'
import { SceneGameEnd } from './SceneGameEnd.js'
import { SceneGameWin } from './SceneGameWin.js'
import { SceneEndCheck } from './SceneEndCheck.js'

const phaserSettings = {
    type: Phaser.AUTO,
    parent: "game",
    width: window.innerWidth,
    height: window.innerHeight,
    backgroundColor: "#000000",
    scene: [ SceneSplash, SceneStartGame, SceneGameSettings, SceneDayStart, SceneDay, SceneBackground, SceneUI, SceneShop, SceneGameEnd, SceneGameWin, SceneEndCheck ],
    dom: {
        createContainer: true
    },
    physics: {
        default: 'arcade'
    },
    pixelArt: true
}

const game = new Phaser.Game(phaserSettings)
window.addEventListener('resize', () => {
    game.scale.resize(window.innerWidth, window.innerHeight)
})

const backgroundSound = new Audio('./sounds/background.wav')
backgroundSound.addEventListener('ended', function() {
    this.currentTime = 0
    this.play()
}, false)
window.addEventListener('focus', () => {
    backgroundSound.play()
})


window.addEventListener('blur', () => {
    backgroundSound.pause()
})

window.addEventListener('sound-off', () => {
    backgroundSound.volume = 0
})

window.addEventListener('sound-on', () => {
    backgroundSound.volume = 1
})
