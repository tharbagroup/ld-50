# Forest Run

Entry for the "Compo" event of Ludum Dare 50.

## Play

Playable at:
* [Gitlab Pages](https://tharbagroup.gitlab.io/ld-50)
* [Itch.io](https://tharbakim.itch.io/forest-run)

## Source Code

Viewable at:
* [Gitlab](https://gitlab.com/tharbagroup/ld-50)

## Ludum Dare Entry

Viewable at:
* [Ludum Dare](https://ldjam.com/events/ludum-dare/50/forest-runner)

## Overiew

### Controls

Movement is done with the arrow keys. `space` is used to interact with surrounding objects.

### Basic Mechanics

In a nutshell, your goal is to make it through sections of forest until you find an appropriate hiding spot to hide from your pursuers. 

The longer you take to clear through each section of forest, the less of a lead you'll have. Once your pursuers catch up to you they'll drag you back into captivity again, until your next attempted escape. Thankfully they're not very smart, and any items you acquire will carry over into your next attempt.

### Win Condition

Although you can run through infinite number of forests if you're quick/lucky enough, it is possible to find a hiding spot and avoid your pursuers. This is not a game mechanic in of itself (if you're trying to hide around a corner, you're going to get bored very quick!) but there are things within the game you must do for this to become available.

### Loss Condition

Although getting caught may feel like a "game over", it's an important part in unlocking the ability to hide from your pursuers. Thankfully you can restart an escape attempt right away - and if you found any way to make your life easier when escaping, you'll still have access tot hose too.

## Post-Release Changelog

* Fixed currency going negative by the Spring Water random event

* Fixed the escape attempt counter not displaying correctly on the game over screen

## Known Issues

* I've seen a couple freezes/infinite loops while generating new maps, but only when playing with a reduced map size in testing. 
* The corner wall pieces at the edges of the map do not render correctly.
